#include <SparkFunMLX90614.h>

#include "BluetoothSerial.h"
#include <Wire.h>
#include <Adafruit_MLX90614.h>

Adafruit_MLX90614 mlx = Adafruit_MLX90614();

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

BluetoothSerial SerialBT;

/*char auth[]   = "P9wUU3b5SmgnDuq_AFJpyH-H5OnQ_fW5";
char ssid[]   = "TP-LINK_AP_E478";
char pass[]   = "10afloor10";
*/

//IRTherm therm;

void setup() {
 Serial.begin(115200);
 //Blynk.begin(auth, ssid, pass);
 SerialBT.begin("ESP32test"); //Bluetooth device name
 Serial.println("The device started, now you can pair it with bluetooth!");
 Serial.println("Adafruit MLX90614 test");  
 mlx.begin(); 
}

void loop() {
   //Blynk.run();
   //timer.run();   
   if (Serial.available()) {
    SerialBT.write(Serial.read());
  }
  if (SerialBT.available()) {
    Serial.write(SerialBT.read());
  }
  delay(1000);
  SerialBT.print(mlx.readObjectTempC()); SerialBT.println("*C");
}
/*
void sendSensor()
{
  Blynk.virtualWrite(V0,therm.object());
  Blynk.virtualWrite(V1,therm.ambient());
}
*/
